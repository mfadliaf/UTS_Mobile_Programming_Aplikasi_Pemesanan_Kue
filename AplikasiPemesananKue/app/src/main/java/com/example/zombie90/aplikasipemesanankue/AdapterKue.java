package com.example.zombie90.aplikasipemesanankue;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ZOMBIE90 on 11/3/2016.
 */

public class AdapterKue extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;

    public AdapterKue(Activity context, String[] itemname, Integer[] imgid) {
        super(context, R.layout.list_kue, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;
        this.imgid=imgid;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_kue, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_namakue);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img_cake);

        txtTitle.setText(itemname[position]);
        imageView.setImageResource(imgid[position]);
        return rowView;

    };
}

