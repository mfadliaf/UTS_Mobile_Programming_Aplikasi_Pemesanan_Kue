package com.example.zombie90.aplikasipemesanankue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MenuKue extends AppCompatActivity {

    public static final String intent_nama = "nama";
    public static final Integer intent_image = 0;

    String[] namakue ={
            "Kue Bajak Laut",
            "Kue Catur",
            "Kue Kamera",
            "Kue Keyboard",
            "Kue Tengkorak",

    };

    Integer[] imgid={
            R.drawable.bajaklaut,
            R.drawable.catur,
            R.drawable.kamera,
            R.drawable.keyboard,
            R.drawable.tengkorak,

    };
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_kue);

        list=(ListView)findViewById(R.id.listcake);
        AdapterKue adapter=new AdapterKue(this,namakue,imgid);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(MenuKue.this, PesanActivity.class);
                intent.putExtra(intent_nama,namakue[position]);
                intent.putExtra(String.valueOf(intent_image), imgid[position]);

                startActivity(intent);

            }
        });
    }
}
